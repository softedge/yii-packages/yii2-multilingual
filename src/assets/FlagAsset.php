<?php

namespace yii\packages\multilingual\assets;

use yii\web\AssetBundle;

class FlagAsset extends AssetBundle
{
    public $css = [ 'css/flag-icons.min.css' ];

    public function init()
    {
        $this->sourcePath = __DIR__ . '/source/form-switcher';
    }
}
