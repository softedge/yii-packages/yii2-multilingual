<?php

namespace yii\packages\multilingual\widgets;

use Yii;
use Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\bootstrap5\ActiveField;
use yii\bootstrap5\ActiveForm as BootstrapActiveForm;
use yii\packages\multilingual\containers\MultilingualFieldContainer;

/**
 * Multilingual ActiveForm
 */
class ActiveForm extends BootstrapActiveForm
{
    /**
     * List of languages.
     * @var array
     */
    public $languages = [];

    /**
     * @var string the default field class name when calling [[field()]] to create a new field.
     */
    public $fieldClass = \yii\packages\multilingual\widgets\ActiveField::class;

    public function init()
    {
        parent::init();
        if (empty($this->languages)) {
            $this->languages = [Yii::$app->language];
            throw new InvalidConfigException('No languages defined');
        }
    }

    /**
     * @param Model $model
     * @param type $attribute
     * @param array $options
     *
     * @return ActiveField|MultilingualFieldContainer
     */
    public function field($model, $attribute, $options = [])
    {
        $fields = [];

        $notMultilingual       = (isset($options['multilingual']) && $options['multilingual'] === false);
        $multilingualField     = (isset($options['multilingual']) && $options['multilingual']);
        $multilingualAttribute = ($model->getBehavior('multilingual') && $model->hasMultilingualAttribute($attribute));

        if (!$notMultilingual && ($multilingualField || $multilingualAttribute)) {

            if ($multilingualAttribute) {
                $languages = array_keys($model->getBehavior('multilingual')->languages);
            } else {
                if (!empty($options['languages'])) {
                    $languages = array_keys($options['languages']);
                } else {
                    $languages = count($this->languages) > 0 ? $this->languages : [Yii::$app->language];
                }
            }

            if (is_array($model->{$attribute})) {
                foreach ($this->languages as $language) {
                    $fields[] = parent::field($model, $attribute . '[' . $language . ']', array_merge($options, ['language' => $language, 'external' => true]));
                }
            } else {
                foreach ($languages as $language) {
                    $fields[] = parent::field($model, $attribute, array_merge($options, ['language' => $language]));
                }
            }

            return new MultilingualFieldContainer(['fields' => $fields]);

        } else {

            return parent::field($model, $attribute, $options);

        }
    }

    /**
     * Renders form language switcher.
     *
     * @param Model $model
     * @param string $view
     * @param array $languages
     * @param string $activeLanguage
     *
     * @return string
     * @throws Exception
     */
    public function languageSwitcher($model, $view = null, $languages = null, $activeLanguage = null)
    {
        if (!is_array($languages)) {
            $languages = ($model->getBehavior('multilingual')) ? $model->getBehavior('multilingual')->languages : $this->languages;
        }

        if ($activeLanguage != null) {
            if (!in_array($activeLanguage, $languages)) {
                // Todo throw error
            }
        } else {
            $activeLanguage = in_array(Yii::$app->language, $languages) ? Yii::$app->language : $languages[0];
        }
        return FormLanguageSwitcher::widget(['languages' => $languages, 'view' => $view, 'activeLanguage' => $activeLanguage]);
    }
}