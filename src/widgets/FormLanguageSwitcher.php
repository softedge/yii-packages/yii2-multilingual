<?php

namespace yii\packages\multilingual\widgets;

use Yii;

/**
 * Widget to display buttons to switch languages in forms
 */
class FormLanguageSwitcher extends \yii\base\Widget
{

    /**
     *
     * @var string view file
     */
    public $view;

    /**
     * List of languages.
     *
     * @var array
     */
    public $languages;

    /**
     * @var string active language
     */
    public $activeLanguage;

    /**
     *
     * @var string default view file
     */
    private $_defaultView = '@vendor/yii-packages/yii2-multilingual/src/views/form-switcher/pills';

    public function init()
    {
        $this->view           = $this->view ?: $this->_defaultView;
        $this->activeLanguage = $this->activeLanguage ?: Yii::$app->language;

        parent::init();
    }

    public function run()
    {
        if ($this->languages) {
            return $this->render($this->view, [
                'language' => $this->activeLanguage,
                'languages' => $this->languages,
            ]);
        }
    }

}
