<?php

namespace yii\packages\multilingual\widgets;

use Yii;
use yii\packages\multilingual\helpers\MultilingualHelper;
use yii\bootstrap5\ActiveField as BootstrapActiveField;
use yii\packages\multilingual\assets\FormLanguageSwitcherAsset;

/**
 * @inheritdoc
 */
class ActiveField extends BootstrapActiveField
{
    /**
     * Language of the field.
     * 
     * @var string 
     */
    public $language;
    
    /**
     * List of languages of the field. For static multilingual fields.
     * 
     * @var string 
     */
    public $languages;

    /**
     * Whether is field multilingual. Use this option to mark an attribute as multilingual
     * in dynamic models.
     * 
     * @var bool 
     */
    public $multilingual = false;

    /**
     * Specify the origin of data
     *
     * @var array
     */
    public $external = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->language && ($this->model->getBehavior('multilingual') || $this->multilingual)) {

            if( !$this->external ) {
                $this->attribute = MultilingualHelper::getAttributeName($this->attribute, $this->language);
            }

            $activeLanguage = (Yii::$app->language === $this->language);
            $switcherUsed = isset(Yii::$app->assetManager->bundles[FormLanguageSwitcherAsset::className()]);

            $this->options = array_merge($this->options, [
                'data-lang' => $this->language,
                'data-toggle' => 'multilingual-field',
                'class' => ($activeLanguage ? 'in' : ''),
                'style' => ((!$activeLanguage && $switcherUsed) ? 'display:none' : ''),
            ]);
        }
    }

}
