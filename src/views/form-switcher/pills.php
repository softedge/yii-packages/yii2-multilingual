<?php
    use yii\helpers\Html;
    use yii\packages\multilingual\helpers\MultilingualHelper;
    use yii\packages\multilingual\assets\FormLanguageSwitcherAsset;

    /* @var $this yii\web\View */
    /* @var $language string */
    /* @var $languages array */

    FormLanguageSwitcherAsset::register($this);
    $associative = count(array_filter(array_keys($languages), 'is_string')) > 0;
?>

<div class="d-flex justify-content-end" style="margin-bottom:-1.5rem;">
    <?php if (count($languages) > 1): ?>
        <ul class="nav nav-pills form-language-switcher">
            <?php if( $associative ): ?>
                <?php foreach ($languages as $key => $value) : ?>
                    <li class="nav-item">
                        <a href="#<?= $key ?>" data-bs-toggle="pill" data-lang="<?= $key ?>" class="<?= 'nav-link' . (($language === $key) ? ' active' : '') ?>">
                            <span class="fi fi-<?= MultilingualHelper::getFlagFromLanguage($key) ?>"></span>
                            <?= $value ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <?php foreach ($languages as $lang) : ?>
                    <li class="nav-item">
                        <a href="#<?= $lang ?>" data-bs-toggle="pill" data-lang="<?= $lang ?>" class="<?= 'nav-link' . (($language === $lang) ? ' active' : '') ?>">
                            <span class="fi fi-<?= MultilingualHelper::getFlagFromLanguage($lang) ?>"></span>
                            <?= $lang ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    <?php endif; ?>
</div>