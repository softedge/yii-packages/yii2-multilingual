<?php
    use yii\helpers\Html;
use yii\packages\multilingual\assets\LanguageSwitcherAsset;
    use yii\packages\multilingual\helpers\MultilingualHelper;

    /* @var $this yii\web\View */
    /* @var $languages */
    /* @var $language */
    /* @var $display */
    /* @var $params */
    /* @var $url */

    LanguageSwitcherAsset::register($this);
?>

<div class="language-switcher language-switcher-pills">
    <ul class="nav nav-pills">
        <?php foreach ($languages as $key => $lang) : ?>
            <?php $title = ($display == 'code') ? $key : $lang; ?>
            <?php if ($language == $key) : ?>
                <li class="nav-item">
                    <a class="nav-link active">
                        <span class="fi fi-<?= MultilingualHelper::getFlagFromLanguage($key) ?>"></span>
                        <?= $title ?>
                    </a>
                </li>
            <?php else: ?>
                <li class="nav-item">
                    <a href="<?= $params[0] ?? $url ?>" data-language="<?= $params['language'] ?? $key ?>" class="nav-link">
                        <span class="fi fi-<?= MultilingualHelper::getFlagFromLanguage($key) ?>"></span>
                        <?= $title ?>
                    </a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>


